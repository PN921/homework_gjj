/*
* 4.33
*
*@author Pavel Novikov
*/

class TaskCh4N33 {
	public static void main (String args[] ) {
		int a;
		a = 2;
		double res;
		res = a % 2;
		if (res == 0)
			System.out.println("The number ends with an even digit");
		if (res != 0)	
			System.out.println("The number ends with an odd digit");
	}
}