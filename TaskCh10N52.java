/*
* 10.52
*
*@author Pavel Novikov
*/

class Recursion {
    int rec(int n) {
        if (n == 0) { 
			return 0;	
		}
		System.out.print(n%10);
			return rec(n / 10);	
	}
	
}

class TaskCh10N52 {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.print("Number 987 back : ");
	t.rec(987);
	}
}