/*
* 9.185
*
*@author Pavel Novikov
*/

class TaskCh9N185 {
	public static void main (String args[] ) {
    String s = "((5+3))*15)/8";
	int k = 0;
	int count = s.length() - 1;
	char x;
	
	for (int i = 0; i<count;i++) {
		x = s.charAt(i);
		if (x == '(')
			k++;
		else if (x == ')')
			k--;
		if (k < 0) {
			System.out.println("You have got extra right brackets. First position:"+i);
			break;
		}
	}
	
	if (k == 0)
		System.out.println("yes");
	else if (k > 0) 
		System.out.println("No, You have got "+k+ " extra left brackets");
	}
}