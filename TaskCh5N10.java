/*
* 5.10
*
*@author Pavel Novikov
*/

import java.util.Scanner;
class TaskCh5N10 {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		float rate;
		int usd[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
		do {
		System.out.print("Enter a rate of dollar: ");
        while (!sc.hasNextFloat()){
			System.out.print("You have entered not an rate of dollar. Enter the rate:");
			sc.next();
			} rate = sc.nextFloat();
		} while (rate < 0);
		for (int x: usd) 
			System.out.println(x+"$ is equal to "+ x*rate +"RUB");
		}
	}