/*
* 9.17
*
*@author Pavel Novikov
*/

class TaskCh9N17 {
	public static void main (String args[] ) {
	String str1 = "Hello world H";
	if (str1.charAt(0) == str1.charAt(str1.length()-1))
		System.out.println("Firs and last symbols in str1 are equal");
	else System.out.println("Firs and last symbols in str1 are not equal");
	}
}