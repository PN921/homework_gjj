/*
* 10.51-B
*
*@author Pavel Novikov
*/

class Recursion {
    int rec(int n) {
        int result;
		result = 0;
		if (n > 0) 
				result = rec(n-1);
				System.out.println(n);
				return result;
	}
	
}

class TaskCh10N51b {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	t.rec(5);
	}
}