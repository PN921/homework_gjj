/*
* 10.44
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n) {
        if (n < 10)  { 
			return n;
		}
		else {
			
			int sum = recurs((n % 10) + (n / 10));
			if (sum > 10){
				return recurs(sum);
			} 
			else { 
				return sum;
			}
		}
		
	}
	
}

class TaskCh10N44 {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Digital root 31337 : "+t.recurs(31337));
	}
}