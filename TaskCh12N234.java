/*
* 12.234
*
*@author Pavel Novikov
*/

import java.util.Scanner; 
class TaskCh12N234{
	public static void main (String args[] ) {
	
	int results[][] = {
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 17, 24, 11, 16, 13, 23, 91, 41, 12, 7 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 },
		{ 15, 14, 10, 11, 12, 6, 90, 45, 13, 5 }
	};
	Scanner sc = new Scanner(System.in);
	System.out.print("Which row you would like to delete? (0..11):");
	int n = sc.nextInt();
	
	System.out.print("Which column you would like to delete? (0..9):");
	int n2 = sc.nextInt();
	
	for(int i = 0; i < 10; i++)
		results[n][i] = 0;
	
	for(int i = 0; i < 12; i++)
		results[i][n2] = 0;
	
	for(int i = 0; i < 12; i++){
		for (int j = 0; j < 10; j++)
			System.out.print(results[i][j] + " ");
		System.out.println();
	}
	}
}