/*
* 10.55
*
*@author Pavel Novikov
*/

import java.util.Scanner; 
class Recursion {
    int recurs(int a, int n) {
        int c;
		int result;
		result=0;
		
		if (a == 0 )
			return 1;
		else 
			result = recurs(a / n,n);
			c = a % n;
			if (n == 16)
			System.out.print(String.format("%x",c));
			else
				System.out.print(c);
			return result;
	}
}

class TaskCh10N55{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	Scanner sc = new Scanner(System.in);
	int n;
	int num;
	System.out.print("enter the number system to convert a number  2<= k <= 16: ");
	n = sc.nextInt();
	System.out.print("enter natural number, which you would like to convert: ");
	num = sc.nextInt();
	System.out.print("Your result in "+n+" system:");
	t.recurs(num,n);
	
	
	}
}