/*
* 6.87
*
*@author Pavel Novikov
*/

import java.util.Scanner;
class TaskCh6N87 {
	public static void main (String args[] ) {
	Scanner sc = new Scanner(System.in);
	String team1 = "";
	String team2 = "";
	int i;
	int exit;
	int team_1,team_2;
	int team_1score,team_2score;
	team_1score = 0;
	team_2score = 0;
	System.out.print("Enter team1:");
	team1 = sc.nextLine();
	System.out.print("Enter team2:");
	team2 = sc.nextLine();
	
	do {
		System.out.print("Enter team to score (1 or 2 or 0 to finish game):");
	while (!sc.hasNextInt()){
			System.out.print("You have entered not an natural number. Enter the number 1 or 2 or 0 to finish game:");
			sc.next();
			} i = sc.nextInt();
			
			if (i == 1){
				do {
					System.out.print("Enter score (1 or 2 or 3):");	
					while (!sc.hasNextInt()) {
					System.out.print("You have entered not an natural number. Enter the number 1 or 2 or 3:");
					sc.next();
					}
					team_1 = sc.nextInt();
					if ((team_1 > 0 ) & (team_1 < 4)) {  
					team_1score = team_1score + team_1; 
					System.out.print(team1 + "-"+team2 + " "+team_1score+":"+team_2score);
					System.out.println("");
					}
					else System.out.println("You are not entered 1 or 2 or 3"); 
				} while ((team_1 < 0) & (team_1 > 3));
			}
			
			
				else if (i == 2){
				do {	
					System.out.print("Enter score (1 or 2 or 3):");	
					while (!sc.hasNextInt()) {
					System.out.print("You have entered not an natural number. Enter the number 1 or 2 or 3:");
					sc.next();
					}
					team_2 = sc.nextInt();
					if ((team_2 > 0 ) & (team_2 < 4)) {
					team_2score = team_2score + team_2;
					System.out.print(team1 + "-"+team2 + " "+team_1score+":"+team_2score);
					System.out.println("");
					}
					else System.out.println("You are not entered 1 or 2 or 3");
				} while ((team_2 < 0) & (team_2 > 3));
			}
				else if ((i !=1) & (i != 2) & (i != 0))
					System.out.println("You are not entered 1 or 2 or 0 ");
		} while (i != 0);
	if (team_1score > team_2score) {
			System.out.println("Winner-"+team1+ " score:"+team_1score + ";");
			System.out.println("Loser-"+team2+ " score:"+team_2score + ";");
	}
	else if (team_1score < team_2score) {
			System.out.println("Loser-"+team1+ " score:"+team_1score);
			System.out.println("Winner-"+team2+ " score:"+team_2score);
	}
	else if (team_1score == team_2score) {
			System.out.println("Nobody win");
			System.out.print(team1 + "-"+team2 + " "+team_1score+":"+team_2score);
	
	}
	}
}