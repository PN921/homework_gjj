/*
*Program that calculates if one of the numbers(x or y) <=2, then True
*@author Pavel Novikov
*@version 1.0
*@return True if only one of the numbers <=2
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29b {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard* */
		int x,y;
		/*Variables int x,y input from keyboard*/
		System.out.print("Enter the number X: ");
        x = sc.nextInt();
		System.out.print("Enter the number Y: ");
        y = sc.nextInt();
	    boolean c = y <= 2;
		boolean d = x <= 2;
		/* If x<=2 = True & y > 2=True Then True */
		if (x <=2) {
			if (!c)
		System.out.println("One of the numbers is <=2");
		else System.out.println("Two of the numbers are <=2");
		}
		/* If y<=2 = True & x > 2=True Then True */
		else if (y <=2) {
				if (!d)
				System.out.println("One of the numbers is <=2");
				else System.out.println("Two of the numbers are <=2");
			}
		}
}