/*
* 2.13
*
*@author Pavel Novikov
*/

import java.util.Scanner; 
class TaskCh2N13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        int n,result,rem;
		result = 0;
		rem = 0;
        System.out.print("Enter the number to reverse: ");
        n = sc.nextInt(); 
        if (n < 200) {
			if (n > 100) {
				while (n > 0) {
				rem = n % 10;
				n = n / 10;
				result = result * 10 + rem;
				}
				System.out.println("Reversed number:" + result);
			}
			else System.out.println("The number out of 200>n>100");
		}
		else System.out.println("The number out of 200>n>100");
    }
}