/*
* 1.3
*
*@author Pavel Novikov
*/
import java.util.Scanner; 
class TaskCh1N3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        int i;
        System.out.print("Enter the number: ");
        i = sc.nextInt(); 
        System.out.println("You entered a number:" + i);
	
    }
}