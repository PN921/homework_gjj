package pack7;

/**
 * Created by pavel on 25.05.2017.
 */
public class HR extends Human {
    private String salute;

    public HR(String name,String salute) {
        super(name);
        this.salute = salute;
    }

    public void greeting(){
        System.out.println(salute);
    }
}
