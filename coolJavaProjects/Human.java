package pack7;

/**
 * Created by pavel on 25.05.2017.
 */
public  abstract class Human {
    private String name;

    public String getName() {return name;}

    public Human(String name){
        this.name = name;
    }
    public abstract void greeting();
}
