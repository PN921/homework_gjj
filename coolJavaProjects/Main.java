package pack7;
import java.util.Random;

/**
 * Created by pavel on 25.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Coder[] coder = new Coder[10];
        HR employer1 = new HR("employer","hi! introduce yourself and describe your java experience please");
        Random random = new Random();

        //Creating 10 coders
        for (int i = 0; i < coder.length; i++) {
            String name = "Programmer"+i;
            if (random.nextInt(2) == 0) {
                coder[i]=new Coder(name,"I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code",false);
            } else coder[i]  = new Coder(name,"I passed successfully getJavaJob exams and code reviews",true);
        }

        //Interview with GetJavaJob candidates
        for (int i=0; i < 10; i++) {
            if (coder[i].isGetJavaJob == true) {
                employer1.greeting();
                coder[i].greeting();
                coder[i].experience();
            }
        }

        //Interview with self-learners candidates
        for (int i=0; i < 10; i++) {
            if (coder[i].isGetJavaJob != true) {
                employer1.greeting();
                coder[i].greeting();
                coder[i].experience();
            }
        }
    }
}