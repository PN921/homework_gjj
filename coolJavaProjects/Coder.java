package pack7;

/**
 * Created by pavel on 25.05.2017.
 */
public class Coder extends Human implements Experience {
    private String experience;
    protected boolean isGetJavaJob;

    public Coder(String name, String experience, Boolean isGetJavaJob){
        super(name);
        this.experience = experience;
        this.isGetJavaJob = isGetJavaJob;
    }

    public  void greeting(){
        System.out.println("hi! my name is " + super.getName());
    };

    public void experience() {
        System.out.println(experience);
        System.out.println("");
    };
}
