/*
* 9.107
*
*@author Pavel Novikov
*/

class TaskCh9N107 {
	public static void main (String args[] ) {
	String s = "Good day!";
	int ch_a = s.indexOf('a');
	int ch_o = s.indexOf('o');
	if ((ch_a == -1) | (ch_o == -1))
		System.out.println("Can't find a or o symbols");
	else if ((ch_a != -1) & (ch_o != -1)){
		StringBuffer new_s = new StringBuffer(s);
		new_s.setCharAt(ch_a,'o');
		new_s.setCharAt(ch_o,'a');
		System.out.println(new_s);
	}
	}
}