/*
* 10.51-V
*
*@author Pavel Novikov
*/

class Recursion {
    int rec(int n) {
        int result;
		result = 0;
		if (n > 0)  
			System.out.println(n);
			result = rec(n - 1);
		
		if (n > 0) 
			System.out.println(n);
			return result;	
	}
	
}

class TaskCh10N51v {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	t.rec(5);
	}
}