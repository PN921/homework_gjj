/*
* 10.45 - B
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n,int a1, int q) {
        if (n== 1)  { 
			return a1;
		}
		else {
			
			 return  a1+recurs(n-1,a1+q,q);
			
			}
		
		
	}
	
}

class TaskCh10N45b{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Sum first N members : "+t.recurs(4,1,2));
	}
}