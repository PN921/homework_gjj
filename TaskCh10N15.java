/*
* 10.50
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n, int m) {
        int res;
		res = 0;
		
		if (n == 0) 
			return  m + 1;
		
		else if (m == 0)     
			return recurs((n - 1),1);
		else return recurs(n - 1,recurs(n,m - 1));
			
	}
	
}

class TaskCh10N15{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Find accerman 1,3 : "+t.recurs(1,3));
	}
}