/*
*Program that calculates if one  of the numbers(x or y) =0 , then True 
*@author Pavel Novikov
*@version 1.0
*@return True if  one of the numbers =0
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29v {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard* */
		int x,y;
		/*Variables int x,y input from keyboard*/
		System.out.print("Enter the number X: ");
        x = sc.nextInt();
		System.out.print("Enter the number Y: ");
        y = sc.nextInt();
	    boolean c = y == 0;
		boolean d = x == 0;
		/* If x=0 = True & y !=0=True Then True */
		if (x == 0) {
			if (!c)
		System.out.println("One of the numbers is =0");
		else System.out.println("Two of the numbers are =0");
		}
		/* If y=0 = True & x != 0 =True Then True */
		else if (y == 0) {
				if (!d)
				System.out.println("One of the numbers is =0");
				else System.out.println("Two of the numbers are =0");
			}
		}
}