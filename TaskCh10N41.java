/*
* 10.41
*
*@author Pavel Novikov
*/

class Factorial {
    int fact(int n) {
        int result;

        if (n == 1)
            return 1;
        result = fact(n - 1) * n;
        System.out.println(n);
		return result;
    }
}

class TaskCh10N41 {
	public static void main (String args[] ) {
	Factorial f = new Factorial();
	System.out.println("Factorial 3 equals:"+f.fact(3));
	}
}