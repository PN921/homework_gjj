/*
* 10.56
*
*@author Pavel Novikov
*/

class Recursion {
    boolean recurs(int n, int i) {
        
		if (n < 2) {
            
			return false;
        } 
        else if (n == 2) {
            
			return true;
        }  
        else if (n % i == 0) {
            
			return false;
        } 
        else if (i < n / 2) {
            return recurs(n, i + 1);
        } else { 
            	return true;
		}
	}
}

class TaskCh10N56{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	boolean number = t.recurs(11,2);
	}
}