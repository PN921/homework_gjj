/*
*Program that calculates whether two numbers is odd at the same time
*@author Pavel Novikov
*@version 1.0
*@return True if two numbers are odd and false if two numbers are even
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29a {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard* */
		int x,y;
		/*Variables to calculate. If the remainder from dividing module 2 is equal to 0 , the number is even*/
		double c,d;
		System.out.print("Enter the number X: ");
        x = sc.nextInt();
		System.out.print("Enter the number Y: ");
        y = sc.nextInt();
	    /*Calculated the remainder of the division modulo 2*/
		c = x % 2;
		/*Calculated the remainder of the division modulo 2*/
		d = y % 2;
		/*If the remainder is not 0 then the number is odd*/
		if (c != 0) {
			/*If you have fulfilled the first condition and the number x is odd, then check the second number*/
			if (d != 0)
		System.out.println("The numbers are odd");
		else System.out.println("One or two numbers are even");
		}
		else System.out.println("One or two numbers are even");
	}
}