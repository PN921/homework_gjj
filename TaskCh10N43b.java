/*
* 10.43 - B
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n) {
        if (n < 10){ 
			return 1;
		}
		else {
			return 1 + recurs(n / 10);
		}
		
	}
	
}

class TaskCh10N43b {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Sum of numbers 125123 : "+t.recurs(125123));
	}
}