/*
* 6.8
*
*@author Pavel Novikov
*/

class TaskCh6N8 {
	public static void main (String args[] ) {
	int nums[] = {1, 2, 4, 8, 64, 4096, 16777216};
	int n = 77;
	for(int x: nums)
		if (x <= n) 
			System.out.println(x);
	}
}