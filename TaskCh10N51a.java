/*
* 10.51-A
*
*@author Pavel Novikov
*/

class Recursion {
    int rec(int n) {
        int result;
		result = 0;
		if (n > 0) 
			System.out.println(n);
			result = rec(n - 1);
			return result;
	}
	
}

class TaskCh10N51a {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	t.rec(5);
	}
}