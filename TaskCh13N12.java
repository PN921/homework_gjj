/*
* 13.12
*
*@author Pavel Novikov
*/

import java.util.Calendar;
import java.text.SimpleDateFormat;

class sotrudnik {
 
    public int id;
    public String surname;
    public String name;
    public String otchestvo;
    public String adress;
	public String data_available;
	    
	 public sotrudnik() {
    }
 
    public sotrudnik(int id, String surname, String name, String otchestvo, String adress, String data_available) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.otchestvo = otchestvo;
        this.adress = adress;
		this.data_available = data_available;

        
    }
	
	 public String getAdress() {
        return adress;
    }
 
    public String getData_available() {
    	return data_available;
	}
	
	 public int getId() {
        return id;
    }
	
	public String getName() {
        return name;
    }
 
    public String getOtchestvo() {
        return otchestvo;
    }
 
    public String getSurname() {
        return surname;
    }
	
	public void setAdres(String adres) {
        this.adress = adress;
    }
	
	 public void setId(int id) {
        this.id = id;
    }
	
	 public void setName(String name) {
        this.name = name;
    }
 
    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }
	
	public void setSurname(String surname) {
        this.surname = surname;
    }
	
	public void show() {
        System.out.println("id: " + getId());
        System.out.println("Surname: " + getSurname());
        System.out.println("Name: " + getName());
        System.out.println("Otchestvo: " + getOtchestvo());
       System.out.println("data avalable: " + getData_available());
        System.out.println("adress: " + getAdress());
    }
	
	public void avail(){
        try {
	
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");;
		Calendar cal = Calendar.getInstance();
		cal.setTime(df.parse(data_available));
		int year = cal.get(cal.YEAR);
		int month = cal.get(cal.MONTH);
		
		//for date now
		Calendar cal2 = Calendar.getInstance();
		cal.getTime();
		int year_now = cal2.get(cal.YEAR);
		int month_now = cal2.get(cal.MONTH);
		
	
	if ((year+3 <= year_now) & (month <= month_now)) {
			System.out.println("id: " + getId());
            System.out.println("Surname: " + getSurname());
            System.out.println("Name: " + getName());
            System.out.println("Otchestvo: " + getOtchestvo());
            System.out.println("adress: " + getAdress());
            
    }
    } catch (Exception exc) {
		
		}
	}
}
class TaskCh13N12{
	public static void main (String args[] ) {
	

	 sotrudnik[] stu = new sotrudnik[20];
        stu[0] = new sotrudnik(1, "Ivan", "Ivanov", "Ivanovich", "Tversjkaya 13","2014/03/26");
        stu[1] = new sotrudnik(2, "Petr", "Petrov", "Petrovich", "Moscow 13","2015/04/22");
        stu[2] = new sotrudnik(3, "Sidr", "Sidorov", "Sidorovich","Stalevarov 10","2011/02/06");
        stu[3] = new sotrudnik(4, "Petr", "Sidorov", "Nikolaevich","Perovo 10","2014/03/26");
	    stu[4] = new sotrudnik(5, "Ivan", "Ivanov", "Ivanovich", "Tversjkaya 13","2016/03/26");
        stu[5] = new sotrudnik(6, "Petr", "Petrov", "Petrovich", "Moscow 13","2015/04/22");
        stu[6] = new sotrudnik(7, "Sidr", "Sidorov", "Sidorovich","Stalevarov 10","2011/02/06");
        stu[7] = new sotrudnik(8, "Petr", "Sidorov", "Nikolaevich","Perovo 10","2015/03/26");
	    stu[8] = new sotrudnik(9, "Ivan", "Ivanov", "Ivanovich", "Tversjkaya 13","2014/03/26");
        stu[9] = new sotrudnik(10, "Petr", "Petrov", "Petrovich", "Moscow 13","2015/04/22");
        stu[10] = new sotrudnik(11, "Sidr", "Sidorov", "Sidorovich","Stalevarov 10","2011/02/06");
        stu[11] = new sotrudnik(12, "Petr", "Sidorov", "Nikolaevich","Perovo 10","2012/03/26");
	    stu[12] = new sotrudnik(13, "Ivan", "Ivanov", "Ivanovich", "Tversjkaya 13","2014/03/26");
        stu[13] = new sotrudnik(14, "Petr", "Petrov", "Petrovich", "Moscow 13","2015/04/22");
        stu[14] = new sotrudnik(15, "Sidr", "Sidorov", "Sidorovich","Stalevarov 10","2011/02/06");
        stu[15] = new sotrudnik(16, "Petr", "Sidorov", "Nikolaevich","Perovo 10","2014/03/26");
	    stu[16] = new sotrudnik(17, "Ivan", "Ivanov", "Ivanovich", "Tversjkaya 13","2009/03/26");
        stu[17] = new sotrudnik(18, "Petr", "Petrov", "Petrovich", "Moscow 13","2010/04/22");
        stu[18] = new sotrudnik(19, "Sidr", "Sidorov", "Sidorovich","Stalevarov 10","2011/02/06");
        stu[19] = new sotrudnik(20, "Petr", "Sidorov", "Nikolaevich","Perovo 10","2014/03/26");
		
	
		
		for (int i = 0; i <= 19; i++) { 
            stu[i].avail();
			System.out.println();
		}
	
	}

}

