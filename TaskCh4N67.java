/*
* 4.67
*
*@author Pavel Novikov
*/

class Day {
	int day;
	int res;
	
	void weekday() {
	res = day % 7;
		if ((res >= 1) & (res <= 5))
			System.out.println("Workday");
		if ((res == 6) | (res == 0))
		System.out.println("Weekend");
	}
}

class TaskCh4N67 {
	public static void main (String args[] ) {
		Day day1 = new Day();
		Day day2 = new Day();
		
		day1.day = 5;
		day2.day = 7;
		
		day1.weekday();
		day2.weekday();
	}
}