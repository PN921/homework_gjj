/*
*Program that calculates if x,y,z <0 , then True 
*@author Pavel Novikov
*@version 1.0
*@return True if  x,y,z < 0
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29g {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard* */
		int x,y,z;
		/*Variables int x,y,z input from keyboard*/
		System.out.print("Enter the number X: ");
        x = sc.nextInt();
		System.out.print("Enter the number Y: ");
        y = sc.nextInt();
		System.out.print("Enter the number Z: ");
        z = sc.nextInt();
	    /* If x<0 & y<0 & z<0 then True  */
		if (x < 0){ 
			if (y < 0){
				if (z < 0)
				System.out.println("All numbers >0");
				else System.out.println("One  of the numbers   > 0");
			}	
			else System.out.println("One  of the numbers > 0");
			}
		else System.out.println("One  of the numbers  > 0");	
		}
}