/*
* 12.25
*
*@author Pavel Novikov
*/

class TaskCh12N25{

static void printMatrix(int[][] grid) {
    for(int r=0; r<grid.length; r++) {
       for(int c=0; c<grid[r].length; c++)
           System.out.print(grid[r][c] + " ");
       System.out.println();
    }
	System.out.println();
}
	
	public static void main (String args[] ) {
	int a[][] = new int[12][10];
	int b[][] = new int[12][10];
	int c[][] = new int[12][10];
	int g[][] = new int[12][10];
	int d[][] = new int[10][12];
	int e[][] = new int[12][10];
	int zh[][] = new int[12][10];
	int z[][] = new int[12][10];
	int ii[][] = new int[12][10];
	int k[][] = new int[12][10];
	int l[][] = new int[12][10];
	int m[][] = new int[12][10];
	int nn[][] = new int[12][10];
	int o[][] = new int[12][10];
	int p[][] = new int[12][10];
	int r[][] = new int[12][10];
	
	int val = 0;
	for (int i=0; i < 12; i++){ 
		for (int j=0; j < 10; j++) {
			val++;
			a[i][j] = val;
			
		}
	
	}
	
	val = 0;
	for (int i=0; i < 12; i++){ 
		int val2=0;
		val++;
		for (int j=0; j < 10; j++) {
			b[i][j] = val+val2;;
			val2=val2+12;
		}
	
	}
	
	val = 10;
	for (int i=0; i < 12; i++){ 
		int val2=val;
		
		for (int j=0; j < 10; j++) {
			c[i][j] = val2;
			val2--;
		}
		val=val+10;
	}
	
	val = 12;
	for (int i=0; i < 12; i++){ 
		int val2=val;
		for (int j=0; j < 10; j++) {
			g[i][j] = val2;
			val2=val2+12;
		}
		val--;;
	}
	
	val = 1;
	int res;
	int n;
	res = 1;
	for (int i=0; i < 10; i++){ 
		n = -1;
		val = val * n;
		for (int j=0; j < 12; j++) {
			d[i][j] = res;
			res = res - val;
		}
		res = res + val + 12;
	}
	
	val = 1;
	res = 1;
	for (int i=0; i < 10; i++){ 
		n = -1;
		val = val * n;
		for (int j=0; j < 12; j++) {
			e[j][i] = res;
			res = res - val;
		}
		res = res + val + 12;
	}
	
	res = 0;
	for (int j=11; j >= 0; j--){ 
		for (int i=0; i < 10; i++) {
			res=res+1;
			zh[j][i] = res;
		}
		
	}
	
	res = 0;
	for (int j=9; j >= 0; j--){ 
		for (int i=0; i < 12; i++) {
			res=res+1;
			z[i][j] = res;
		}
		
	}

	res = 0;
	for (int j=11; j >= 0; j--){ 
		for (int i=9; i >= 0; i--) {
			res=res+1;
			ii[j][i] = res;
		}
		
	}
	
	res = 0;
	for (int i=9; i >= 0; i--){ 
		for (int j=11; j >= 0; j--) {
			res=res+1;
			k[j][i] = res;
		}
		
	}
	
	val = 1;
	res = 1;
	for (int i=11; i >= 0; i--){ 
		n = -1;
		val = val * n;
		for (int j=0; j < 10; j++) {
			l[i][j] = res;
			res = res - val;
		}
		res = res + val + 10;
	}
	
	val = 1;
	res = 1;
	for (int i=0; i < 12; i++){ 
		n = -1;
		val = val * n;
		for (int j=9; j >=0; j--) {
			m[i][j] = res;
			res = res - val;
		}
		res = res + val + 10;
	}
	
	val = 1;
	res = 1;
	for (int j=9; j >=0; j--){ 
		n = -1;
		val = val * n;
		for (int i=0; i <12; i++) {
			nn[i][j] = res;
			res = res - val;
		}
		res = res + val + 12;
	}
	
	val = 1;
	res = 1;
	for (int j=0; j < 10; j++){ 
		n = -1;
		val = val * n;
		for (int i=11; i >=0; i--) {
			o[i][j] = res;
			res = res - val;
		}
		res = res + val + 12;
	}
	
	val = 1;
	res = 1;
	for (int j=11; j >=0 ; j--){ 
		n = -1;
		val = val * n;
		for (int i=9; i >=0; i--) {
			p[j][i] = res;
			res = res - val;
		}
		res = res + val + 10;
	}
	
	val = 1;
	res = 1;
	for (int j=9; j >=0 ; j--){ 
		n = -1;
		val = val * n;
		for (int i=11; i >=0; i--) {
			r[i][j] = res;
			res = res - val;
		}
		res = res + val + 12;
	}
	

	printMatrix(a);
	printMatrix(b);
	printMatrix(c);
	printMatrix(g);
	printMatrix(d);
	printMatrix(e);
	printMatrix(zh);
	printMatrix(z);
	printMatrix(ii);
	printMatrix(k);
	printMatrix(l);
	printMatrix(m);
	printMatrix(nn);
	printMatrix(o);
	printMatrix(p);
	printMatrix(r);

	}
}