/*
* 1.17 -s
*
*@author Pavel Novikov
*/

import static java.lang.Math.*;

class TaskCh1N17s {
	public static void main (String args[] ) {
		int x;
		x = -1;
		int res = abs(x) + abs(x+1);
		System.out.println("Result:" + res);
		}
}


