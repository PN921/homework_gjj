/*
* 11.158
*
*@author Pavel Novikov
*/
import java.util.Scanner; 
import java.util.ArrayList;
class TaskCh11N158{
	public static void main (String args[] ) {
	ArrayList<Integer> nums = new ArrayList<Integer>();
	nums.add(0,88);
	nums.add(1,1);
	nums.add(2,1);
	nums.add(3,55);
	nums.add(4,2);
	nums.add(5,55);
	nums.add(6,7);
	nums.add(7,7);
	nums.add(8,8);
	nums.add(9,55);
	nums.add(10,90);

	//Remove third element
	nums.remove(3);
	//Remove "k" element
	Scanner sc = new Scanner(System.in);
	System.out.print("Which element do you like to delet?(0...9):");
	int n = sc.nextInt();
	
	for (int i=0; i < nums.size(); i++) {
		for (int ii=0; ii < nums.size(); ii++) {
			if ((i != ii) & (nums.get(i) == nums.get(ii)))
			nums.remove(ii);
		}
	}
	System.out.println("Massive after removal identical elements:" + nums);
	}
}