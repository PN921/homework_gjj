/*
* 12.63
*
*@author Pavel Novikov
*/

class TaskCh12N63{
	public static void main (String args[] ) {
	int school[][] = new int[11][4];

	school[0][0] = 28;
	school[0][1] = 23;
	school[0][2] = 22;
	school[0][3] = 27;
	
	school[1][0] = 28;
	school[1][1] = 23;
	school[1][2] = 21;
	school[1][3] = 21;

	school[2][0] = 24;
    school[2][1] = 13;
    school[2][2] = 22;
    school[2][3] = 25;	
	
	school[3][0] = 21;
	school[3][1] = 22;
	school[3][2] = 25;
	school[3][3] = 28;
	
	school[4][0] = 45;
	school[4][1] = 23;
	school[4][2] = 12;
	school[4][3] = 67;
	
	school[5][0] = 89;
	school[5][1] = 28;
	school[5][2] = 56;
	school[5][3] = 2;
	
	school[6][0] = 23;
	school[6][1] = 12;
	school[6][2] = 7;
	school[6][3] = 9;
	
	school[7][0] = 7;
	school[7][1] = 56;
	school[7][2] = 34;
	school[7][3] = 5;
	
	school[8][0] = 12;
	school[8][1] = 4;
	school[8][2] = 8;
	school[8][3] = 2;
	
	school[9][0] = 77;
	school[9][1] = 54;
	school[9][2] = 23;
	school[9][3] = 8;
	
	school[10][0] = 23;
	school[10][1] = 2;
	school[10][2] = 8;
	school[10][3] = 20;
	
	int summa;
	int number = 0;
		for(int i=0; i < 11; i++){
		summa = 0;
		number++;
		for(int j=0; j < 4; j++) {
		summa = summa + school[i][j];
	}
	System.out.println("the average number of students in "+ number + " per class: " + summa/4);
	}
	}
}