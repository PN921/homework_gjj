/*
* 4.115
*
*@author Pavel Novikov
*/

import java.util.Scanner;
class TaskCh4N115 {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		int year;
		String animal = "";
		String color = "";
		
		do {
		System.out.print("Enter the year: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an natural number. Enter the year:");
			sc.next();
			} year = sc.nextInt();
		} while (year < 0);
	
		int res1 = year % 12;
		switch (res1) {
			case 0:
				animal = "monkey";
				break;
			case 1:
				animal = "cock";
				break;
			case 2:
				animal = "dog";
				break;
			case 3:
				animal = "pig";
				break;
			case 4:
				animal = "rat";
				break;
			case 5:
				animal = "cow";
				break;
			case 6:
				animal = "tiger";
				break;
			case 7:
				animal = "hare";
				break;
			case 8:
				animal = "dragon";
				break;
			case 9:
				animal = "snake";
				break;
			case 10:
				animal = "horse";
				break;
			case 11:
				animal = "lamb";
				break;
		
		}
		
		int res2 = year % 10;
		switch (res2) {
			case 0:
			case 1:
				color = "white";
				break;
			case 2:
			case 3:
				color = "black";
				break;
			case 4:
			case 5:
				color = "green";
				break;
			case 6:
			case 7:
				color = "red";
				break;
			case 8:
			case 9:
				color = "yellow";
				break;
			
		}
		System.out.println(animal + "," + color);
	}
}