/*
* 10.46-B
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n,int a1, int q) {
        if (n== 1)  { 
			return a1;
		}
		else {
			
			 return  recurs(n-1,a1*q,q)+a1;
			
			}
		
		
	}
	
}

class TaskCh10N46b{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Find the sum for 4 member geometry progression, first member 1, step q 2 : "+t.recurs(4,1,2));
	}
}