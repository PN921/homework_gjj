/*
* 1.17 - r
*
*@author Pavel Novikov
*/

import static java.lang.Math.*;

class TaskCh1N17r {
	public static void main (String args[] ) {
		int x;
		x = 1;
		double res = (sqrt(x+1)+sqrt(x-1))/2*sqrt(x);
		System.out.println("Result:" + res);
		}
}


