/*
* 1.17 - o
*
*@author Pavel Novikov
*/
import static java.lang.Math.*;

class TaskCh1N17o {
	public static void main (String args[] ) {
		double x,res;
		x = 45;
		res = sqrt(1-pow(sin(toRadians(x)),2));
		System.out.println("Result:" + res);
	}
}