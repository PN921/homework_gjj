/*
* 10.47
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n) {
        if (n < 2)  { 
			return 1;
		}
		else {
			
			 return  (recurs(n-1)+recurs(n-2));
			
			}
		
		
	}
	
}

class TaskCh10N47{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Find Fibonacchi for 4 : "+t.recurs(4));
	}
}