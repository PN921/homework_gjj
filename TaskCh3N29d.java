/*
*Program that calculates if only one of the numbers x,y,z <=3 , then True 
*@author Pavel Novikov
*@version 1.0
*@return True if  only one of the numbers x,y,z <= 3
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29d {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard * */
		int x,y,z;
		boolean run = true;
		/*Variables int x,y,z input from keyboard with input test*/
		do {
		System.out.print("Enter the number X: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number X:");
			sc.next();
		} x = sc.nextInt();
		run = false;
		} while (run); 
		
		do {
		System.out.print("Enter the number Y: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number Y:");
			sc.next();
		}y = sc.nextInt();
		run = false;
		} while (run);
		do {
		System.out.print("Enter the number Z: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number Z:");
			sc.next();
		} z = sc.nextInt();
		run = false;
		} while (run);
	    boolean b = x <=3;
		boolean c = y <=3;
		boolean d = z <=3;
		/* If (x<=3) & !(y <=3)& !(z<=3) then True or  !(x<=3) & (y <=3)& !(z<=3) then True or !(x<=3) & !(y <=3)& (z<=3)  */
		if (b & !c & !d)
				System.out.println("Only one of the numbers <=3");
			else if (!b & c & !d)
				System.out.println("Only one of the numbers <=3");
					else if (!b & !c & d)
						System.out.println("Only one of the numbers <=3");
							else System.out.println("More than one number <=3");
		}
}