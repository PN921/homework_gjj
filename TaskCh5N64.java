/*
* 5.64
*
*@author Pavel Novikov
*/

class TaskCh5N64 {
	public static void main (String args[] ) {
	int sum_people = 0;
	int sum_ploshad = 0;
	int res;
	int people[];
	people = new int[12];
	people[0] = 300;
	people[1] = 44;
	people[2] = 345;
	people[3] = 123;
	people[4] = 99;
	people[5] = 56;
	people[6] = 34;
	people[7] = 23;
	people[8] = 567;
	people[9] = 445;
	people[10] = 323;
	people[11] = 98;
	
	int ploshad[];
	ploshad = new int[12];
	ploshad[0] = 2;
	ploshad[1] = 1;
	ploshad[2] = 7;
	ploshad[3] = 6;
	ploshad[4] = 8;
	ploshad[5] = 24;
	ploshad[6] = 22;
	ploshad[7] = 33;
	ploshad[8] = 7;
	ploshad[9] = 34;
	ploshad[10] = 9;
	ploshad[11] = 2;
	
	for(int x: people) sum_people+=x;
	for(int y: ploshad) sum_ploshad+=y;
	
	System.out.println("The average population density for the region as a whole:"+sum_people/sum_ploshad);
	}
	
}