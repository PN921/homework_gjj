/*
* 10.48
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int[] a, int index) {
        if (index < 0)  { 
			return a[0];
		}
		else {
			return Math.max(a[index-1], recurs(a, index-2));
			
			}
		
		
	}
	
}

class TaskCh10N48{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	int[] array1 = {0,14,345,900,12,23423,23432,343,3434343,34,4545,656,6767,23,1223123};
	System.out.println("Max number of massive : "+t.recurs(array1,array1.length));
	}
}