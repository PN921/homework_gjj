/*
* 2.43
*
*@author Pavel Novikov
*/

import java.util.Scanner; 

class TaskCh2N43 {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		int a,b;
		System.out.print("Enter the number a: ");
        a = sc.nextInt();
		System.out.print("Enter the number b: ");
        b = sc.nextInt();
		int res = (a%b)+(b%a)+ 1;
		System.out.println("Result:" + res);
		}
}