/*
* 10.45 - A
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n,int a1, int q) {
        if (n== 1)  { 
			return 1;
		}
		else {
			
			 return  q+recurs(n-1,a1,q);
			
			}
		
		
	}
	
}

class TaskCh10N45a{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Find the 4 member progression, first member 1, step q 2 : "+t.recurs(4,1,2));
	}
}