/*
* 4.36
*
*@author Pavel Novikov
*/

class Signal {
	int t;
	int res;
	
	void color() {
	res = t % 5;
	if ((res >= 0) & (res <= 2))
		System.out.println("Green if " + t);
	else if ((res >= 3) & (res <= 4))
		System.out.println("Red if " +t);
	}
}
 

class TaskCh4N36 {
	public static void main (String args[] ) {
	Signal signal1 = new Signal();
	Signal signal2 = new Signal();
	
	signal1.t = 3;
	signal2.t = 5;
	
	signal1.color();
	signal2.color();
	
	}
}