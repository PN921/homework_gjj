/*
* 10.53
*
*@author Pavel Novikov
*/

import java.util.Scanner; 
class Recursion {
    int reverseArray(int[] a, int start, int end) {
        int temp;
		if (start >= end)
			return start;
		temp = a[start];
		a[start] = a[end];
		a[end] = temp;
		 return reverseArray(a,start+1,end-1);
	}
	
}

class TaskCh10N53{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	int[] array1 = new int[10];
	Scanner sc = new Scanner(System.in);
	int n;
	System.out.println("You can enter 10 number, or 0 for exit");
	for (int i = 0; i <10; i++){
	System.out.println("Enter the number "+i+":");
	n = sc.nextInt();
	if (n == 0) break;
	array1[i] = n;
	}
	System.out.print("Reverse numbers: ");
	t.reverseArray(array1, 0, 9);
	
	for (int i = 0; i < 10; i++)
		System.out.print(array1[i] + " ");
		System.out.println("");
	}
}