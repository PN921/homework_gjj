/*
* 4.15
*
*@author Pavel Novikov
*/

class Age {
	byte tmonth;
	int tyear;
	byte bmonth;
	int byear;
	int a;
	
	void allages() {
	if (tmonth < bmonth)
		a = -1;
			else a = 0;
	System.out.print("Your age: ");
	System.out.println((tyear - byear)+a);
	}
}
 

class TaskCh4N15 {
	public static void main (String args[] ) {
	Age myage1 = new Age();
	Age myage2 = new Age();
	Age myage3 = new Age();
	
	myage1.tmonth = 12;
	myage1.tyear = 2014;
	myage1.bmonth = 6;
 	myage1.byear = 1985;
	
	myage2.tmonth = 5;
	myage2.tyear = 2014;
	myage2.bmonth = 6;
	myage2.byear = 1985;
	
	myage3.tmonth = 6;
	myage3.tyear = 2014;
	myage3.bmonth = 6;
	myage3.byear = 1985;
	
	myage1.allages();
	myage2.allages();
	myage3.allages();
	}
}