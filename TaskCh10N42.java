/*
* 10.42
*
*@author Pavel Novikov
*/

class Recursion {
    int Step(int a, int n) {
        int result;
		result = 0;
		if (n == 0) 
			return 1;
		
        if (n == 1)
            return a;
        if (n > 0) 
		    result = a*Step(a,n-1);
			return result;
		
	}
	
}

class TaskCh10N42 {
	public static void main (String args[] ) {
	Recursion exp = new Recursion();
	System.out.println("exponentiation 2 number 3 : "+exp.Step(3,2));
	}
}