/*
* 2.39
*
*@author Pavel Novikov
*/

import java.util.Scanner; 
class TaskCh2N39 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        int h,m,s;
		double result;
		System.out.print("Enter the h(hour): ");
        h = sc.nextInt();
		System.out.print("Enter the m(minute): ");
        m = sc.nextInt();
		System.out.print("Enter the s(second): ");
        s = sc.nextInt(); 
        if ((h > 0 & h <= 23) & (m >= 0 & m <= 59) & (s>=0 & s <= 59)) {
			if (h > 12) 
			h-=12;
			result = (h * 30) + (m * 0.5) + s * (0.5/60) ;
			System.out.println("The angle is:" + result);
			}
			else System.out.println("Not subject to condition 0<h<=23,0<=m<=59,0<=s<=59");
		}
}