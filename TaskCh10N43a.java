/*
* 10.43 - A
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int n) {
        if (n < 10){ 
			return n;
		}
		else {
			return n % 10 + recurs(n / 10);
		}
		
	}
	
}

class TaskCh10N43a {
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	System.out.println("Sum of numbers 125 : "+t.recurs(125));
	}
}