/*
* 1.17 - p
*
*@author Pavel Novikov
*/

import static java.lang.Math.*;

class TaskCh1N17p {
	public static void main (String args[] ) {
		int a,x,b,c;
		a = 1;
		x = 1;
		b = 1;
		c = 1;
		double res = 1 / sqrt(a * pow(x,2) + b * x + c);
		System.out.println("Result:" + res);
	}
}