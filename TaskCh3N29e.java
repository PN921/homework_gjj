/*
*Program that calculates if  one of the numbers x,y,z > 100 , then True 
*@author Pavel Novikov
*@version 1.0
*@return True if  if one of the numbers x,y,z >100
*/
/*Import the library java.util.Scaner ,than help for us input from the keyboard*/

import java.util.Scanner;

class TaskCh3N29e {
	public static void main (String args[] ) {
		Scanner sc = new Scanner(System.in);
		/*It will be the integers entered from the keyboard* */
		int x,y,z;
		boolean run = true;
		/*Variables int x,y,z input from keyboard with input test*/
		do {
		System.out.print("Enter the number X: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number X:");
			sc.next();
		} x = sc.nextInt();
		run = false;
		} while (run); 
		
		do {
		System.out.print("Enter the number Y: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number Y:");
			sc.next();
		}y = sc.nextInt();
		run = false;
		} while (run);
		do {
		System.out.print("Enter the number Z: ");
        while (!sc.hasNextInt()){
			System.out.print("You have entered not an integer number. Enter the number Z:");
			sc.next();
		} z = sc.nextInt();
		run = false;
		} while (run);
	    boolean b = x > 100;
		boolean c = y > 100;
		boolean d = z > 100;
		/* If x | y | z then True */
		if (b | c | d)
				System.out.println("One or two or three numbers > 100");
			else System.out.println("No one  of the numbers > 100");
					
		}
}