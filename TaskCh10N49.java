/*
* 10.49
*
*@author Pavel Novikov
*/

class Recursion {
    int recurs(int[] a, int index) {
        int ind;
		int temp = index - 1;
		if (temp == 0)  { 
			return temp;
		}
		else {
			ind = recurs(a,temp);
			return a[temp] > a[ind] ? temp : ind;
			}
	}
	
}

class TaskCh10N49{
	public static void main (String args[] ) {
	Recursion t = new Recursion();
	int[] array1 = {0,14,345,900,12,23423,23432,343,3434343,34,4545,656,6767,23,1223123};
	System.out.println("Index of max number of massive : "+t.recurs(array1,array1.length));
	}
}